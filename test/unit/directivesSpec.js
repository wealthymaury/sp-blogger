'use strict';

/* jasmine specs for directives go here */

// describe('directives', function() {
//   beforeEach(module('myApp.directives'));

//   describe('app-version', function() {
//     it('should print current version', function() {
//       module(function($provide) {
//         $provide.value('version', 'TEST_VER');
//       });
//       inject(function($compile, $rootScope) {
//         var element = $compile('<span app-version></span>')($rootScope);
//         expect(element.text()).toEqual('TEST_VER');
//       });
//     });
//   });
// });

beforeEach(module('spBlogger.posts.directives'));
beforeEach(module('spBlogger.admin.services'));
beforeEach(module('templates'));

describe('Directive Test\n', function(){
	it('Should initialize comments div with 2 comments', inject(function($rootScope, $compile){
		var $scope = $rootScope.$new();
		$scope.singlePost = {
			comments: [
				{content: 'test', author: 'test'},
				{content: 'test1', author: 'test1'},
			]
		};
		var template = '<spb-comments post-instance="singlePost"></spb-comments>';
		var elem = angular.element(template);
		$compile(elem)($scope);
		$rootScope.$digest();
		expect(elem.find('.single-comment').length).toBe(2);
	}));
});