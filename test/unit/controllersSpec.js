'use strict';

/* jasmine specs for controllers go here */

// describe('controllers', function(){
//   beforeEach(module('myApp.controllers'));

//   it('should ....', inject(function() {
//     //spec body
//   }));

//   it('should ....', inject(function() {
//     //spec body
//   }));
// });

beforeEach(module('spBlogger.posts.controllers'));
beforeEach(module('spBlogger.posts.services'));
beforeEach(module('spBlogger.admin.services'));
beforeEach(module('ngResource'));
beforeEach(module('ui.router'));

describe('PostController Test\n', function(){
	// esto funciono cuando use sin Backend real
	// beforeEach(module('spBlogger.posts.controllers'));
	// beforeEach(module('spBlogger.posts.services'));

	// it('Should initialize controller with 4 posts', inject(function ($rootScope, $controller, postService) {
	// 	var $scope = $rootScope.$new();
	// 	$controller('PostController', { $scope: $scope, postService: postService });
	// 	expect($scope.posts.length).toBe(4);
	// }));

	// testeando con backend real
	// aqui se simula una peticion a un server y la respuesta
	var $httpBackend;
	beforeEach(inject(function (_$httpBackend_) {
		$httpBackend = _$httpBackend_;
		$httpBackend.expectGET('http://spblogger-sitepointdemos.rhcloud.com/api/posts').respond([
			{title: 'Test1', _id:1},
			{title: 'Test2', _id:2},
		]);
	}));

	it('Should initialize controller with 2 posts', inject(function ($rootScope, $controller, Post) {
		var $scope = $rootScope.$new();
		$controller('PostController', { $scope: $scope, Post: Post });
		$httpBackend.flush(); // corriendo el backend falso
		expect($scope.posts.length).toBe(2);
	}));
});

describe('PostDetailController Test\n', function(){
	// esto funciono cuando use sin Backend real
	// beforeEach(module('spBlogger.posts.controllers'));
	// beforeEach(module('ui.router'));
	// beforeEach(module('spBlogger.posts.services'));

	// it('Should initialize controller with 1 post', inject(function ($state, $stateParams, $rootScope, $controller, postService) {
	// 	var $scope = $rootScope.$new();
	// 	$stateParams.id = 2;
	// 	$controller('PostDetailController', { $scope: $scope, $stateParams: $stateParams, $state: $state, postService: postService });
	// 	expect($scope.singlePost).not.toBe(undefined);
	// }));

	// testeando con backend real
	var $httpBackend;
	beforeEach(inject(function (_$httpBackend_) {
		$httpBackend = _$httpBackend_;
		$httpBackend.expectGET('http://spblogger-sitepointdemos.rhcloud.com/api/posts/100').respond({title: 'Test100', _id: 100});
	}));

	it('Should initialize controller with 1 post', inject(function ($state, $stateParams, $rootScope, $controller, Post) {
		var $scope = $rootScope.$new();
		$stateParams.id = 100;
		$controller('PostDetailController', {
			$stateParams: $stateParams,
			$state: $state,
			$scope: $scope,
			Post: Post
		});

		$httpBackend.flush();
		expect($scope.singlePost).not.toBe(undefined);
	}));
});