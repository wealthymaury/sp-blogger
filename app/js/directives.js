'use strict';

/* Directives */


angular.module('spBlogger.directives', [])
	.directive('appVersion', ['version', function(version) {
		return function(scope, elm, attrs) {
			elm.text(version);
		};
	}])
	.directive('helloWorld', function () {
		return {
			restrict: 'AEC',
			replace: true,
			template: '<h3>Hello, world!</h3>'
		};
	});