'use strict';

angular.module('spBlogger', [
	'ui.router',
	'ngResource',
	'ngCookies',
	
	'spBlogger.filters',
	'spBlogger.services',
	'spBlogger.directives',
	'spBlogger.controllers',

	'spBlogger.posts',
	'spBlogger.admin',
	'pascalprecht.translate'
])
.run(['$state', '$rootScope', '$translate', function ($state, $rootScope, $translate) {
	$state.go('allPosts');

	$rootScope.languagePreference = { currentLanguage: 'es' };
	$rootScope.languagePreference.switchLanguage = function(key){
		$translate.use(key);
		$rootScope.languagePreference.currentLanguage = key;
	}

}])
.config(['$translateProvider', '$httpProvider', function ($translateProvider, $httpProvider){
	$translateProvider.translations('en', {
		TITLE: 'The Single Page Blogger',
		SUBTITLE: 'One Stop Blogging Solution',
		COMMENTS: 'Comments',
		BY: 'By',
		ADD: 'Add'
	});

	$translateProvider.translations('es', {
		TITLE: 'Un Blog Single Page',
		SUBTITLE: 'One Stop Blogging Solution',
		COMMENTS: 'Comentarios',
		BY: 'Por',
		ADD: 'Agregar'
	});

	$translateProvider.preferredLanguage('es');

	// esto es lo ultimo del libro (pagina 302)
	$httpProvider.defaults.withCredentials = true;
}]);
