'use strict';

angular.module('spBlogger.admin', [
	'spBlogger.admin.services',
	'spBlogger.admin.controllers',
	'spBlogger.admin.filters',
])
.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('admin', {
			url: '/admin',
			abstract: true, //este estado no puede ser cargado directamente, necesita cargarse con uno de sus hijos
			controller: 'AdminController',
			templateUrl: 'modules/admin/views/admin-home.html',
			//control de acceso, user llegaria al controller como dependencia
			resolve: {
				user: ['authService', '$q', function (authService, $q){
					return authService.user || $q.reject({ unAuthorized: true });
				}]
			}
		})
		.state('admin.postNew', { //el poner admin.algo indica que es un estado hijo de admin, por lo que cargan juntos
			url: '/posts/new',
			controller: 'PostCreationController',
			templateUrl: 'modules/admin/views/admin-new-post.html'
		})
		.state('admin.postUpdate', {
			url: '/posts/:id/edit',
			controller: 'PostUpdateController',
			templateUrl: 'modules/admin/views/admin-update-post.html'
		})
		.state('admin.postViewAll', {
			url: '', //indica que es el estado por defecto
			controller: 'PostListController',
			templateUrl: 'modules/admin/views/admin-all-posts.html'
		})
		.state('login', {
			url: '/login',
			controller: 'LoginController',
			templateUrl: 'modules/admin/views/login.html',
			resolve: {
				user: ['authService', '$q', function (authService, $q){
					if(authService.user){
						return $q.reject({authorized: true});
					}
				}]
			}
		});
}])
.run(['$rootScope', '$state', '$cookieStore', 'authService', function ($rootScope, $state, $cookieStore, authService){
	$rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){
		if(error.unAuthorized) {
			$state.go('login');
		}else if(error.authorized){
			$state.go('admin.postViewAll');
		}
	});

	// para casos de page refresh
	authService.user = $cookieStore.get('user');
}]);


