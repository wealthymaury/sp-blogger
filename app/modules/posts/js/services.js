'use strict';

angular.module('spBlogger.posts.services', [])
	.factory('postService', function () {
		return {
			posts: [
				{
					id: 1,
					title: 'Simple test 1',
					content: 'Sample content...',
					permalink: 'simple-title1',
					author: 'Sandeed',
					datePublished: '2012-04-04'
				},
				{
					id: 2,
					title: 'Simple test 2',
					content: 'Sample content...',
					permalink: 'simple-title2',
					author: 'Sandeed',
					datePublished: '2012-04-04'
				},
				{
					id: 3,
					title: 'Simple test 3',
					content: 'Sample content...',
					permalink: 'simple-title3',
					author: 'Sandeed',
					datePublished: '2012-04-04'
				},
				{
					id: 4,
					title: 'Simple test 4',
					content: 'Sample content...',
					permalink: 'simple-title4',
					author: 'Sandeed',
					datePublished: '2012-04-04'
				},
			],
			getAll: function () {
				return this.posts;
			},
			getPostById: function (id) {
				for (var i in this.posts) {
					if(this.posts[i].id == id) {
						return this.posts[i];
					}
				}
			}
		}
	})
	.factory('commentService', function() {
		return {
			deleteComment : function (id){
	  			debugger;
	  		}
		}
	});
