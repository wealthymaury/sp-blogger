'use strict';

angular.module('spBlogger.posts.directives', [
	'spBlogger.posts.services'
])
.directive('spbComments',['Post', function(Post) {
	return {
		restrict: 'AEC',
		scope: {
			postInstance: '='
		},
		replace: true,
		link: function (scope, elem, attrs){
			scope.saveComment = function(){
				var postID = scope.postInstance._id; 
				var savedPostInstance = {};
				scope.comment.datePublished = new Date();
		
				angular.copy(scope.postInstance, savedPostInstance);
				savedPostInstance.comments.unshift(scope.comment);
				savedPostInstance.$update(function (data, getHeaders) {
					scope.postInstance.comments.unshift(scope.comment)
					scope.comment = {};
				});
			}
		},
		templateUrl: 'modules/posts/views/comments.html'
	}
}])
.directive('spbComment', ['commentService' ,function (commentService){
	return {
		restrict: 'AEC',
		scope: {
			comment: '=',
		},
		replace: true,
		templateUrl: 'modules/posts/views/singleComment.html',
		link: function(scope, elem, attrs){
			scope.deleteComment = function(){
				commentService.deleteComment(scope.comment._id);
			}
		}
	}
}]);
